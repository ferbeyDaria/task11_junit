package com.ferbey;

import com.ferbey.model.LongestPlateau;
import org.junit.jupiter.api.Test;
import org.junit.runners.Parameterized;

public class LongestPlateauTest {
    private int[] arr={34, 2, 5, 6, 6, 6, 1, 2, 2, 9};
    private int count=0;

    @Parameterized.Parameter(0)
    public int el1;
    @Parameterized.Parameter(1)
    public int el2;
    @Parameterized.Parameter(2)
    public int el3;
    @Parameterized.Parameter(3)
    public int el4;
    @Parameterized.Parameter(4)
    public int el5;
    @Parameterized.Parameter(5)
    public int el6;
    @Parameterized.Parameter(6)
    public int el7;

    @Test
    public void testArray() {
        int[] arr = new int[]{5, 6, 6, 6, 6, 6, 6, 6, 6, 2, 3, 5};
        LongestPlateau lp = new LongestPlateau();
        lp.longest(arr);
    }

}
