package com.ferbey;

import com.ferbey.model.Minesweeper;
import org.junit.jupiter.api.Test;

public class MinesweeperTest {

    @Test
    public void test1(){
        Minesweeper minesweeper = new Minesweeper(20, 20, 1.2);
        System.out.println(minesweeper.getAreaString());
        minesweeper.replacePeriods();
        System.out.println(minesweeper.getCharAreaString());
    }
}
