package com.ferbey.view;

import com.ferbey.controller.Controller;
import com.ferbey.model.LongestPlateau;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {
    private static Logger logger = LogManager.getLogger(MyView.class);
    private Controller controller = new Controller();
    private Map<String, String> menu;
    private Map<String, Printable> methodMenu;
    private Scanner input = new Scanner(System.in);

    private void setMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", "1 - Longest plateau");
        menu.put("2", "2 - Minesweeper");
        menu.put("Q", "Q - exit ");

    }

    public MyView() {
        methodMenu = new LinkedHashMap<>();

        setMenu();
        methodMenu.put("1", this::task1);
        methodMenu.put("2", this::task2);
    }

    private void task1() {
        controller.getLP();
    }

    private void task2() {
        logger.info(controller.getMS());
    }

    //--------------------------------------------------------------------
    private void outputMenu() {
        System.out.println("\nMenu");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String menuKey;
        do {
            outputMenu();
            System.out.print("Please, select menu point: ");
            menuKey = input.nextLine().toUpperCase();
            try {
                methodMenu.get(menuKey).print();
            } catch (Exception e) {
            }
        } while (!menuKey.equals("Q"));
    }
}
