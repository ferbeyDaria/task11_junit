package com.ferbey.view;

public interface Printable {
    void print();
}
