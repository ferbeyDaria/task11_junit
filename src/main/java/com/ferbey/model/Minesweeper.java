package com.ferbey.model;

public class Minesweeper {
    private final int M;
    private final int N;
    private final double p;
    private boolean[][] area;
    private char[][] charArea;

    public Minesweeper(int M, int N, double p) {
        this.M = M;
        this.N = N;
        this.p = p;
        area = new boolean[M][N];
        charArea = new char[M][N];
        fillArea();
    }

    private void fillArea() {
        for (int i = 0; i < M; i++) {
            for (int j = 0; j < N; j++) {
                area[i][j] = Math.random() > p;
            }
        }
    }

    private void fillCharArea() {
        for (int i = 0; i < M; i++) {
            for (int j = 0; j < N; j++) {
                if (area[i][j]) charArea[i][j] = '*';
                else charArea[i][j] = '.';
            }
        }
    }

    public String getAreaString() {
        String result = "";
        for (int i = 0; i < M; i++) {
            for (int j = 0; j < N; j++) {
                if (area[i][j]) result += "* ";
                else result += ". ";
            }
            result += "\n";
        }
        return result;
    }

    public void replacePeriods() {
        for (int i = 0; i < M; i++) {
            for (int j = 0; j < N; j++) {
                if (!area[i][j]) charArea[i][j] = Character.forDigit(calculateBombs(i, j), 10);
                else charArea[i][j] = '*';
            }
        }
    }

    private int calculateBombs(int i, int j) {
        int result = 0;

        if (i + 1 < M) {
            if (area[i + 1][j]) result++;
            if (j + 1 < N) {
                if (area[i + 1][j + 1]) result++;
            }
        }

        if (j + 1 < N) {
            if (area[i][j + 1]) result++;
        }

        if (j - 1 >= 0) {
            if (area[i][j - 1]) result++;
            if(i+1<M) {
                if (area[i+1][j - 1]) result++;
            }
        }

        if (i - 1 >= 0) {
            if (j - 1 > 0) {
                if (area[i - 1][j - 1]) result++;
            }
            if (j + 1 < M) {
                if (area[i - 1][j + 1]) result++;
            }

            if (area[i - 1][j]) result++;
        }

        return result;
    }

    public String getCharAreaString() {
        String result = "";
        for (int i = 0; i < M; i++) {
            for (int j = 0; j < N; j++) {
                if (!area[i][j]) result += charArea[i][j] + " ";
                else result += "* ";
            }
            result += "\n";
        }
        return result;
    }


}
