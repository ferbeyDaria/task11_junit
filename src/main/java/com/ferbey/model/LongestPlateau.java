package com.ferbey.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class LongestPlateau {
    private static Logger logger = LogManager.getLogger(LongestPlateau.class);
    List<Integer> b = new ArrayList<>();


    public void longest(int[] arr){

        boolean t = false;
        int temp = 0;
        int loc =0;
        int k = 1;
        int length = 1;
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] == arr[i - 1]) {
                t = true;
                temp = arr[i];
                k++;
                loc=i;
                length= Math.max(length,k);
            } else {
                t = false;
            }
            if (!t && k > 1) {
                for (int j = 0; j < k; j++) {
                    if(arr[loc+1]<temp && arr[loc-length]<temp){
                        b.add(temp);
                    }

                }
                k = 1;
            }
        }
        logger.info("Length of the longest plateau:  "+length);
        logger.info("Location of the longest plateau"+"["+(loc-length+1)+";"+loc+"]");
        logger.info("Longest plateau: "+b);
    }

}
