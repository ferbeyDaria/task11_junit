package com.ferbey.controller;

import com.ferbey.model.LongestPlateau;
import com.ferbey.model.Minesweeper;

public class Controller {
    LongestPlateau lp;
    Minesweeper ms;
    private int[] arr;

    public Controller(){
        lp=new LongestPlateau();
        ms=new Minesweeper(20,20,1.2);
        arr= new int[]{34, 2, 5, 2, 2, 2, 4, 6, 6, 6, 3};
    }

    public void getLP() {
        lp.longest(arr);
    }

    public String getMS() {
         return (ms.getCharAreaString());
    }
}
